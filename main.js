
function getAllGitLabProjects() {



    var httpRequest = new XMLHttpRequest();

    if (!httpRequest) {

        alert('Giving up : ( Cannot create an XMLHTTP instance! ');

        return false;

    }


    httpRequest.onreadystatechange = alertContents;

    httpRequest.open('GET', 'https://gitlab.com/api/v4/projects');

    httpRequest.send();


    function alertContents() {

        if (httpRequest.readyState == XMLHttpRequest.DONE) {

            if (httpRequest.status == 200) {

                var count = 0;


                var getAllProjects = JSON.parse(httpRequest.responseText);

                var getAllProjectsDiv = document.getElementById('content');

                var projectItem = '';


                getAllProjects.forEach((item, index) => {

 

                    projectItem += `<div class="col-sm-12 col-md-4 col-lg-4 mb-5 ">

                    <div class="card" style="box-shadow: 8px 8px 16px 8px rgba(0,0,0,0.2);">

                   

                       <div class="card-body" style="background-color:#ffe6ff">

                            <h5 class="card-title" style="overflow: hidden;

                            white-space: nowrap;

                            text-overflow: ellipsis;

                            max-width: 450px;"><strong style="color:#002b80">Project Id: </strong>${item.id}</h5>

                            <p class="card-title" style="overflow: hidden;

                            display: -webkit-box;

                            -webkit-line-clamp: 6;

                            -webkit-box-orient: vertical;

                            text-overflow: ellipsis;

                            word-wrap: normal;

                            max-height: 50px;

                            min-height: 50px;

                            "><strong style="color:#002b80">Name: </strong>${item.name}</p>

                            <p class="card-title" style="

                            overflow: hidden;

                            display: -webkit-box;

                            -webkit-line-clamp: 6;

                            -webkit-box-orient: vertical;

                            text-overflow: ellipsis;

                            word-wrap: normal;

                            max-height: 150px;

                            min-height: 150px;

                            "><strong style="color:#002b80">Description: </strong>${item.description}</p>

                            <p class="card-title" style="overflow: hidden;

                            display: -webkit-box;

                            -webkit-line-clamp: 6;

                            -webkit-box-orient: vertical;

                            text-overflow: ellipsis;

                            word-wrap: normal;

                            max-height: 50px;

                            min-height: 50px;

                            "><strong style="color:#002b80">Path: </strong>${item.name_with_namespace}</p>

                            <p class="card-title"><strong style="color:#002b80">Created Date: </strong>${item.created_at}</p>

                            <p class="card-title"><strong style="color:#002b80">Default Branch: </strong>${item.default_branch}</p>

                            <p class="card-title" style="overflow: hidden;

                            display: -webkit-box;

                            -webkit-line-clamp: 6;

                            -webkit-box-orient: vertical;

                            text-overflow: ellipsis;

                            word-wrap: normal;

                            max-height: 100px;

                            min-height: 100px;

                            "><strong style="color:#002b80">SSH URL: </strong>${item.ssh_url_to_repo}</p>

                            <p class="card-title" style="overflow: hidden;

                            display: -webkit-box;

                            -webkit-line-clamp: 6;

                            -webkit-box-orient: vertical;

                            text-overflow: ellipsis;

                            word-wrap: normal;

                            max-height: 100px;

                            min-height: 100px;

                            "><strong style="color:#002b80">HTTP URL: </strong>${item.http_url_to_repo}</p>

                            <p class="card-title" style="overflow: hidden;

                            display: -webkit-box;

                            -webkit-line-clamp: 6;

                            -webkit-box-orient: vertical;

                            text-overflow: ellipsis;

                            word-wrap: normal;

                            max-height: 100px;

                            min-height: 100px;

                            "><strong style="color:#002b80">Web URL: </strong>${item.web_url}</p>

                            <p class="card-title"><strong style="color:#002b80">Last Activities: </strong>${item.last_activity_at}</p>

                           

                        </div>

                     </div>

                    </div>`;

                });

 

                getAllProjectsDiv.innerHTML = projectItem;

            } else {

                alert("There is a problem with request!");

            }

        }

    }

}



function getAllGitLabGroups() {


    var httpRequest = new XMLHttpRequest();

    if (!httpRequest) {

        alert('Giving up : ( Cannot create an XMLHTTP instance! ');

        return false;

    }

    httpRequest.onreadystatechange = alertContents;

    httpRequest.open('GET', 'https://gitlab.com/api/v4/groups');

    httpRequest.send();


    function alertContents() {

        if (httpRequest.readyState == XMLHttpRequest.DONE) {

            if (httpRequest.status == 200) {

                var count = 0;

                var gitLabGroups = JSON.parse(httpRequest.responseText);

                var gitLabGroupsDiv = document.getElementById('content');

                var groupItem = '';

                gitLabGroups.forEach((item, index) => {

 

                    groupItem += `<div class="col-sm-12 col-md-4 col-lg-4 mb-5 ">

                    <div class="card" style="box-shadow: 8px 8px 16px 8px rgba(0,0,0,0.2);">

                   

                       <div class="card-body" style="background-color:#ffe6ff">

                            <h5 class="card-title" style="overflow: hidden;

                            white-space: nowrap;

                            text-overflow: ellipsis;

                            max-width: 450px;"><strong style="color:#002b80">Project Id: </strong>${item.id}</h5>

                            <p class="card-title" style="overflow: hidden;

                            display: -webkit-box;

                            -webkit-line-clamp: 6;

                            -webkit-box-orient: vertical;

                            text-overflow: ellipsis;

                            word-wrap: normal;

                            max-height: 50px;

                            min-height: 50px;

                            "><strong style="color:#002b80">Name: </strong>${item.name}</p>

                            <p class="card-title" style="

                            overflow: hidden;

                            display: -webkit-box;

                            -webkit-line-clamp: 6;

                            -webkit-box-orient: vertical;

                            text-overflow: ellipsis;

                            word-wrap: normal;

                            max-height: 150px;

                            min-height: 150px;

                            "><strong style="color:#002b80">Description: </strong>${item.description}</p>

                            <p class="card-title" style="overflow: hidden;

                            display: -webkit-box;

                            -webkit-line-clamp: 6;

                            -webkit-box-orient: vertical;

                            text-overflow: ellipsis;

                            word-wrap: normal;

                            max-height: 50px;

                            min-height: 50px;

                            "><strong style="color:#002b80">Path: </strong>${item.path}</p>

                            <p class="card-title"><strong style="color:#002b80">Created Date: </strong>${item.created_at}</p>

                            <p class="card-title"><strong style="color:#002b80">Default Branch Protection: </strong>${item.default_branch_protection}</p>

                            <p class="card-title"><strong style="color:#002b80">Visibility: </strong>${item.visibility}</p>

                            <p class="card-title"><strong style="color:#002b80">Project Creation Level: </strong>${item.project_creation_level}</p>

                            <p class="card-title" style="overflow: hidden;

                            display: -webkit-box;

                            -webkit-line-clamp: 6;

                            -webkit-box-orient: vertical;

                            text-overflow: ellipsis;

                            word-wrap: normal;

                            max-height: 100px;

                            min-height: 100px;

                            "><strong style="color:#002b80">Web URL: </strong>${item.web_url}</p>

                           </div>

                     </div>

                    </div>`;

                });

 

                gitLabGroupsDiv.innerHTML = groupItem;

            } else {

                alert("Error with request!");

            }

        }

    }

}

 

async function getAllUser() {

    var url = 'https://gitlab.com/api/v4/users';

    const response = await fetch(url, {

        method: 'GET',

        headers: {

            'PRIVATE-TOKEN': 'glpat-VodSZ4MK1dmVA-L44Kqw',

            'Content-Type': 'application/json',

        },

    });

    const apptestJsonData = await response.json();

 

    var gitLabUsersDiv = document.getElementById('content');

    var userItem = '';



    apptestJsonData.forEach((item, index) => {

 

        userItem += `<div class="col-sm-12 col-md-4 col-lg-4 mb-5 ">

                    <div class="card" style="box-shadow: 8px 8px 16px 8px rgba(0,0,0,0.2);">

 

                       <div class="card-body" style="background-color:#ffe6ff">

                       <img class="card-img-top mb-2" style="height:250px;"

                    src="${item.avatar_url}"

                    alt="Card image cap">

                            <h5 class="card-title" style="overflow: hidden;

                            white-space: nowrap;

                            text-overflow: ellipsis;

                            max-width: 450px;"><strong style="color:#002b80">Project Id: </strong>${item.id}</h5>

                            <p class="card-title" style="overflow: hidden;

                            display: -webkit-box;

                            -webkit-line-clamp: 2;

                            -webkit-box-orient: vertical;

                            text-overflow: ellipsis;

                            word-wrap: normal;

                             "><strong style="color:#002b80">Name: </strong>${item.name}</p>

                            <p class="card-title"><strong style="color:#002b80">Username: </strong>${item.username}</p>

                            <p class="card-title"><strong style="color:#002b80">State: </strong>${item.state}</p>

                            <p class="card-title" style="overflow: hidden;

                            display: -webkit-box;

                            -webkit-line-clamp: 6;

                            -webkit-box-orient: vertical;

                            text-overflow: ellipsis;

                            word-wrap: normal;

                            max-height: 50px;

                            min-height: 50px;

                             "><strong style="color:#002b80">Web URL: </strong>${item.web_url}</p>

                           </div>

                     </div>

                    </div>`;

    });

    gitLabUsersDiv.innerHTML = userItem;

 

}



async function getBroadCastMsg() {

    var url = 'https://gitlab.com/api/v4/broadcast_messages';

    const response = await fetch(url, {

        method: 'GET',

        headers: {

            'PRIVATE-TOKEN': 'glpat-VodSZ4MK1dmVA-L44Kqw',

            'Content-Type': 'application/json',

        },

    });

    const apptestJsonData = await response.json();

 

    var broadCastMsgDiv = document.getElementById('content');

    var userItem = '';

    apptestJsonData.forEach((item, index) => {

 

        userItem += `<div class="col-sm-12 col-md-4 col-lg-4 mb-5 ">

                    <div class="card" style="box-shadow: 8px 8px 16px 8px rgba(0,0,0,0.2);">

 

                       <div class="card-body" style="background-color:#ffe6ff">

                            <h5 class="card-title" style="overflow: hidden;

                            white-space: nowrap;

                            text-overflow: ellipsis;

                            max-width: 450px;"><strong style="color:#FAEBD7">Project Id: </strong>${item.id}</h5>

                            <p class="card-title" style="overflow: hidden;

                            display: -webkit-box;

                            -webkit-line-clamp: 6;

                            -webkit-box-orient: vertical;

                            text-overflow: ellipsis;

                            word-wrap: normal;

                            max-height: 150px;

                            min-height: 150px;

                             "><strong style="color:#FAEBD7">Message: </strong>${item.message}</p>

                            <p class="card-title"><strong style="color:#FAEBD7">Start Date: </strong>${item.starts_at}</p>

                            <p class="card-title"><strong style="color:#FAEBD7">End Date: </strong>${item.ends_at}</p>

                            <p class="card-title" style="overflow: hidden;

                            display: -webkit-box;

                            -webkit-line-clamp: 6;

                            -webkit-box-orient: vertical;

                            text-overflow: ellipsis;

                            word-wrap: normal;

                            max-height: 50px;

                            min-height: 50px;

                             "><strong style="color:#FAEBD7">Broadcast type: </strong>${item.broadcast_type}</p>

                           </div>

                     </div>

                    </div>`;

    });

    broadCastMsgDiv.innerHTML = userItem;

 

}